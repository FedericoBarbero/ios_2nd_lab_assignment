//
//  ViewController.swift
//  BabyGame
//
//  Created by Federico Barbero on 03/11/15.
//  Copyright © 2015 eurecom. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet var MainView: UIView!
    @IBOutlet weak var rect: UIButton!
    @IBOutlet weak var triang: UIButton!
    @IBOutlet weak var square: UIButton!
    @IBOutlet weak var oval: UIButton!
    @IBOutlet weak var circle: UIButton!
    @IBOutlet weak var trianSlot: UIButton!
    @IBOutlet weak var circleSlot: UIButton!
    @IBOutlet weak var squareSlot: UIButton!
    @IBOutlet weak var rectSlot: UIButton!
    @IBOutlet weak var ovalSlot: UIButton!
    @IBOutlet weak var winButon: UIButton!
    @IBOutlet weak var house: UIImageView!

    var rectoriginalPosition: CGPoint?
    var ovaloriginalPosition: CGPoint?
    var circleoriginalPosition: CGPoint?
    var trianoriginalPosition: CGPoint?
    var squareoriginalPosition: CGPoint?
    
    
    var target: UIButton!
    var source: UIButton!
    var count: Int32!
    var originalPosition: CGPoint!
    
    var backgroundMusic: AVAudioPlayer?
    
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        //1
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.becomeFirstResponder()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let backgroundMusic = self.setupAudioPlayerWithFile("background_music", type:"mp3") {
            self.backgroundMusic = backgroundMusic
        }
        
        backgroundMusic?.volume = 0.3
        backgroundMusic?.play()
        
        let background = UIColor.init(patternImage: UIImage(named: "background")!)
        self.viewIfLoaded?.backgroundColor = background
        
        rect.tag = 1
        triang.tag = 2
        square.tag = 3
        oval.tag = 4
        circle.tag = 5
        
        rectSlot.tag = 6
        trianSlot.tag = 7
        squareSlot.tag = 8
        ovalSlot.tag = 9
        circleSlot.tag = 10

        house.layer.zPosition = 1
        rect.layer.zPosition = 2
        triang.layer.zPosition = 2
        square.layer.zPosition = 2
        oval.layer.zPosition = 2
        circle.layer.zPosition = 2
        
        /*
        rectoriginalPosition = rect.center
        trianoriginalPosition = triang.center
        squareoriginalPosition = square.center
        ovaloriginalPosition = oval.center
        circleoriginalPosition = circle.center
        */
        
        count = 0
    }
    
    func reset() {
        UIView.animateWithDuration(0.5, animations: {
            if let pos:CGPoint = self.rectoriginalPosition{
                self.rect.center = pos
            }
        })
        UIView.animateWithDuration(0.5, animations: {
            if let pos:CGPoint = self.trianoriginalPosition{
                self.triang.center = pos
            }
        })
        UIView.animateWithDuration(0.5, animations: {
            if let pos:CGPoint = self.squareoriginalPosition{
                self.square.center = pos
            }
        })
        UIView.animateWithDuration(0.5, animations: {
            if let pos:CGPoint = self.ovaloriginalPosition{
                self.oval.center = pos
            }
        })
        UIView.animateWithDuration(0.5, animations: {
            if let pos:CGPoint = self.circleoriginalPosition{
                self.circle.center = pos
            }
        })
        
        count = 0
        rectoriginalPosition = nil
        trianoriginalPosition = nil
        squareoriginalPosition = nil
        ovaloriginalPosition = nil
        circleoriginalPosition = nil
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.resignFirstResponder()
        super.viewWillDisappear(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func canBecomeFirstResponder() -> Bool {
        return true
    }

    @IBAction func buttonPressed(sender: UIButton) {
        source = sender
        sender.layer.zPosition = 3
        UIView.animateWithDuration(0.5, animations: {
            self.source.transform = CGAffineTransformMakeScale(1.3, 1.3)
        })
        
        switch sender.tag
        {
        case rect.tag:
            target = rectSlot
            rectoriginalPosition = sender.center
        case square.tag:
            target = squareSlot
            squareoriginalPosition = sender.center
        case oval.tag:
            target = ovalSlot
            ovaloriginalPosition = sender.center
        case triang.tag:
            target = trianSlot
            trianoriginalPosition = sender.center
        case circle.tag:
            target = circleSlot
            circleoriginalPosition = sender.center
        default:
            return
        }
        
        originalPosition = sender.center
        //self.
        //MainView.bringSubviewToFront(sender)
        //label.text = String(target.center)
    }
    
    @IBAction func buttonMoved(sender: UIButton, forEvent event: UIEvent) {
        
        let touches = event.allTouches()
            for touch in touches!
            {
                sender.center = touch.locationInView(self.viewIfLoaded)
            }
        //label.text = String(sender.center)
    }
    
    @IBAction func buttonReleased(sender: UIButton) {
        UIView.animateWithDuration(0.5, animations: {
            self.source.transform = CGAffineTransformMakeScale(1.0, 1.0)
        })
        sender.layer.zPosition = 2
        if(distanceBetweenPoint(sender.center, point2: target.center) < 50)
        {
            UIView.animateWithDuration(0.5, animations: {
                self.source.center = self.target.center
            })
            count!++
            if count == 5
            {
                let alert = UIAlertController(title: "WIN", message: "Congratulations! You won the game!", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: {(let alert:UIAlertAction) in
                    print("handled")
                    self.winButon.hidden = false
                }))
                self.presentViewController(alert, animated: true, completion: nil)
                
                
            }
        }
        else
        {
            UIView.animateWithDuration(0.7, animations: {
                self.source.center = self.originalPosition
            })
            
        }
    }
    
    @IBAction func winButtonPressed(sender: UIButton) {
        sender.hidden = true
        self.reset()
        updateViewConstraints()
        self.view.layoutIfNeeded()
    }
    
    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
        if motion == UIEventSubtype.MotionShake
        {
            //MainView.bringSubviewToFront(winButon)
            reset()
        }
    }
    
    private func distanceBetweenPoint(point1:CGPoint, point2:CGPoint) ->CGFloat
    {
        let dx: CGFloat = point2.x - point1.x
        let dy: CGFloat = point2.y - point1.y
        
        return sqrt(dx*dx+dy*dy)
    }

}

