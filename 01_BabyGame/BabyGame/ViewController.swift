//
//  ViewController.swift
//  BabyGame
//
//  Created by Federico Barbero on 03/11/15.
//  Copyright © 2015 eurecom. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var slot: UIImageView!
    var originalPosition: CGPoint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let background = UIColor.init(patternImage: UIImage(named: "background")!)
        self.viewIfLoaded?.backgroundColor = background
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonPressed(sender: UIButton) {
        originalPosition = button.center
    }
    
    @IBAction func buttonMoved(sender: UIButton, forEvent event: UIEvent) {
       
        //print(event.description)
        /*if let touches = event.allTouches()
        {*/
        let touches = event.allTouches()
            for touch in touches!
            {
                button.center = touch.locationInView(self.viewIfLoaded)
            }       
        //}
    
    }
    
    @IBAction func buttonReleased(sender: UIButton) {
        if(distanceBetweenPoint(button.center, point2: slot.center) < 100)
        {
            button.center = slot.center
        }
        else
        {
            button.center = originalPosition
        }
    }
    
    private func distanceBetweenPoint(point1:CGPoint, point2:CGPoint) ->CGFloat
    {
        let dx: CGFloat = point2.x - point1.x
        let dy: CGFloat = point2.y - point1.y
        
        return sqrt(dx*dx+dy*dy)
    }

}

